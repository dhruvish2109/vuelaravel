/*!
 * Independent Components for Jquery Plugin or Others
 * By Tymk Softwares
 */
Vue.component('vue-datepicker', {
    props: ['value', 'name'],
    watch: {
        value: function(new_value) {
            this.input_value = new_value ? moment(new_value).format('DD MMM Y') : null
        }
    },
    data: function () {
        return {
            input_value: this.value ? moment(this.value).format('DD MMM Y') : null
        }
    },
    template: `<input type="text" ref="datePicker" class="form-control" :name="name" :value="input_value" readonly>`,
    mounted: function() {
        let self = this;
        $(this.$el).datepicker({
            autoclose: true,
            todayHighlight: true,
            format: "dd M yyyy",
        }).on('changeDate', function(e) {
            self.$emit('input', e.target.value)
        });
    }
});



Vue.component('vue-counter-button', {
    props: ['value', 'min', 'max'],
    watch: {
        value: function (newVal) {
            this.counter = newVal;
        }
    },
    data: function () {
        return {
            counter: this.value,
            min_value: this.min,
            max_value: this.max
        }
    },
    template: `<div class="counter-component">
        <div class="input-group " style="margin-top: 0.5rem">
        <span class="input-group-append">
        <button class="reduced items-count btn-dark" type="button" @click="decrement"><i class="la la-minus"></i></button>
        </span>
        <input type="text" class="form-control text-center" title="Qty" :value="counter.current" maxlength="12" readonly>
        <span class="input-group-append">
        <button class="increase items-count btn-dark" type="button" @click="increment"><i class="la la-plus"></i></button>
        </span>
        </span>
        </div>
        </div>
    `,
    methods: {
        increment: function () {

            if (this.max_value > this.counter.current) {
                this.counter.current += 1;
                this.$emit('input', {action: 'increment', current: this.counter.current, actual: this.counter.actual})
            }
            else {
                swal("Oops", "This item has not enough stock, Support Admin", "error");
            }

        },
        decrement: function () {

            if (this.counter.current > this.min_value) {
                this.counter.current -= 1;
                this.$emit('input', {action: 'decrement', current: this.counter.current, actual: this.counter.actual})
            }
        }
    }
});

Vue.component('vue-web-counter-button', {
    props: ['value', 'min', 'max', 'productPriceId'],
    watch: {
        value: function (newVal) {
            this.counter = newVal;
        }
    },
    data: function () {
        return {
            counter: this.value,
            min_value: this.min,
            max_value: this.max,
            priceId: this.productPriceId,
        }
    },
    template: `<div class="product-quantity mb-2 mt-2">
                    <span class="minus product-quantity-minus items-count reduced" @click="decrement"><i class="fa fa-minus"></i></span>
                    <input type="number" class="num text-center" title="Qty" :value="counter.current" maxlength="12" readonly></span>
                    <span class="plus items-count increase product-quantity-plus" @click="increment"><i class="fa fa-plus"></i></span>
               </div>`,
    methods: {
        increment: function () {

            if (this.max_value > this.counter.current) {
                this.counter.current += 1;

                this.$emit('input', {
                    action: 'increment',
                    current: this.counter.current,
                    productPriceId: this.priceId
                });

                this.$emit('change-counter-btn', {
                    action: 'increment',
                    current: this.counter.current,
                    productPriceId: this.priceId
                });
            }
            else {
                swal("Oops", "This item has not enough stock, Support Admin", "error");
            }

        },
        decrement: function () {

            if (this.counter.current > this.min_value) {
                this.counter.current -= 1;

                this.$emit('input', {
                    action: 'decrement',
                    current: this.counter.current,
                    productPriceId: this.priceId
                });

                this.$emit('change-counter-btn', {
                    action: 'decrement',
                    current: this.counter.current,
                    productPriceId: this.priceId
                });
            }
        }
    }
});

Vue.component('vue-filer', {
    props: ['value', 'limit', 'maxSize', 'extensions', 'showThumb', 'addMore', 'name'],
    template: '<input type="file" :name="nameValue">',
    data: function () {
        return {
            nameValue: this.name ? this.name : 'image',
            filerInput: null,
            limitValue: this.limit ? this.limit : 1,
            maxSizeValue: this.maxSize ? this.maxSize : 2,
            allowExt: this.extensions ? this.extensions : ['jpg', 'jpeg', 'png'],
            displayThumbs: this.showThumbs ? this.showThumbs : true,
            addMoreValue: this.addMore === '0' ? false : true
        }
    },
    watch: {
        value: function (newValue, oldValue) {
            // $(this.$el).val(value).trigger('change')
        }
    },
    mounted: function () {

        let self = this;

        $(this.$el).filer({
            limit: self.limitValue, maxSize: self.maxSizeValue, extensions: self.allowExt, showThumbs: self.displayThumbs, addMore: self.addMoreValue,
            changeInput: true,
            onSelect: function () {

                setTimeout(() => {

                    let images = $('.jFiler-item-thumb-image').map(function() {
                        let imageSource = $(this).children('img').attr('src');
                        return imageSource.replace(/^data:image\/[a-z]+;base64,/, "");
                    });

                    self.$emit('input', images);

                }, 500);
            },
            onRemove: function () {

                setTimeout(() => {

                    let images = $('.jFiler-item-thumb-image').map(function() {
                        let imageSource = $(this).children('img').attr('src');
                        return imageSource.replace(/^data:image\/[a-z]+;base64,/, "");
                    });

                    self.$emit('input', images);

                }, 500);
            }
        });

    }
});

Vue.component('vue-select2', {
    props: ['options', 'value', 'allowSearch', 'placeHolder', 'emptyAfterSelect','name'],
    data: function () {
        return {

            select_options: this.options,
            doEmptyAfterSelect: this.emptyAfterSelect ? this.emptyAfterSelect : 0
        }
    },
    template: `<select class="full-width" :name="name" >
               <option value="" v-if="placeHolder">{{ placeHolder }}</option>
               <option v-for="select_option in select_options" :value="select_option.value">{{ select_option.label }}</option>
               </select>`,
    mounted: function () {

        let self = this;

        $(this.$el).select2({ minimumResultsForSearch: this.allowSearch }).val(this.value).trigger('change').on('select2:select', function (event) {

            self.$emit('input', event.target.value);
            self.eventOnChange(event.target.value);

            if (parseInt(self.doEmptyAfterSelect) === 1) {
                setTimeout(() => {
                    $(self.$el).select2().val('').trigger('change');
                }, 1500);
            }

        });

    },
    watch: {
        value: function (value) {
             // $(this.$el).val(value).trigger('change')
        },
        options: function (options) {
            $(this.$el).empty().select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    },
    methods: {
        eventOnChange(value) {
            this.$emit('change-select2', value);
        }
    }
});

Vue.component('vue-multi-select2', {
    props: ['options', 'value', 'allowSearch', 'placeHolder', 'emptyAfterSelect','name'],
    data: function () {
        return {

            select_options: this.options,
            doEmptyAfterSelect: this.emptyAfterSelect ? this.emptyAfterSelect : 0
        }
    },
    template: `<select class="full-width" :name="name" multiple >
               <option v-for="select_option in select_options" :value="select_option.value">{{ select_option.label }}</option>
               </select>`,
    mounted: function () {

        let self = this;

        $(this.$el).select2({ minimumResultsForSearch: this.allowSearch }).val(this.value).trigger('change').on('select2:select', function (event) {

            self.$emit('input', event.target.value);
            self.eventOnChange(event.target.value);

            if (parseInt(self.doEmptyAfterSelect) === 1) {
                setTimeout(() => {
                    $(self.$el).select2().val('').trigger('change');
                }, 1500);
            }

        });

    },
    watch: {
        value: function (value) {
            // $(this.$el).val(value).trigger('change')
        },
        options: function (options) {
            $(this.$el).empty().select2({ data: options })
        }
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    },
    methods: {
        eventOnChange(value) {
            this.$emit('change-select2', value);
        }
    }
});


/*!
 * Global Filters
 * By Tymk Softwares
 */
Vue.filter('currency', function (value) {
    return new Intl.NumberFormat("en-IN", {style: "currency", currency: "INR"}).format(value);
});
Vue.filter('capitalize', function (value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1)
});

Vue.filter('slugify', function (value) {
    return value.toString().toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-').replace(/^-+/, '').replace(/-+$/, '');
});

Vue.filter('date', function (value, argument) {
    return moment(value).format(argument);
});

Vue.filter('str_limit', function (string, value) {
    if (string) {
        return string.length <= value ? string : string.substring(0, value) + '...';
    }
    return string;
});
