<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VueJs Laravel</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container mt-5" id="crudApp">
    <h1 class="text-center">VueJs Crud With Laravel</h1>
    <div class="row">
        <div class="col-md-6 mt-2">
            <form id="createForm" method="post" @submit.prevent="createUpdateUser()">
                {{ csrf_field()  }}
                <input type="hidden" v-model="form.id" id="id" value="">
                <div class="form-group">
                    <label for="">UserName</label>
                    <input type="text" v-model="form.username" id="username" class="form-control" placeholder="Enter the Username">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" v-model="form.email"  id="email" class="form-control" placeholder="Enter the Email">
                </div>
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password"  v-model="form.password" id="password" class="form-control" placeholder="Enter the Password">
                </div>
                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                <div id="message" v-show="message"></div>
                {{--<div class="alert alert-success alert-dismissible">--}}
                {{--<button type="button" class="close" data-dismiss="alert">&times;</button>--}}
                {{--<strong>Success!</strong>--}}
                {{--</div>--}}
            </form>
        </div>
        <div class="col-md-6 mt-5">
            <table class="table table-bordered table-striped">
                <thead class="text-center">
                <tr>
                    <td>#</td>
                    <td>UserName</td>
                    <td>Email</td>
                    <td colspan="2">Action</td>
                </tr>
                </thead>
                <tbody class="text-center">
                <tr v-for="(user,index) in users">
                    <td>@{{ index + 1 }}</td>
                    <td>@{{ user.username }}</td>
                    <td>@{{ user.email }}</td>
                    <td>
                        <button type="button" class="btn btn-warning btn-sm rounded-0" @click="editUser(user.id)">Update</button>
                        <button type="button" class="btn btn-danger btn-sm rounded-0" @click="deleteUser(user.id)">Delete</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{{ asset('Plugins/axios/axios.min.js') }}"></script>
<script src="{{ asset('Plugins/vue/vue.js') }}"></script>
<script>
    Vue.prototype.$http = axios;

    new Vue({
        el: '#crudApp',
        data:{
            users : {},
            form : {
                id: '',
                username: '',
                email: '',
                password: '',
            },
            message: false,
        },
        methods:{
            getUsers: function () {
                this.$http.get('{{ route("get-users")}}').then(response => {
                    if (response.data.status === 'success') {
                        this.users = response.data.users;
                    }
                });
            },
            createUpdateUser: function () {
                if(this.form.id === ''){
                    this.$http.post('{{ route('user-create')  }}',this.form)
                        .then(response => {
                            if(response.data.status === true){
                                this.message = '<div class="alert alert-success alert-dismissible">'+
                                    '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                                    '<strong>'+ response.data.message +'</strong>'+
                                    '</div>';
                                this.message = true;
//                                window.setTimeout(function () {
//                                    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
////                                    $(this).remove();
//                                        $('#message-box').remove();
//                                    });
//                                },2000);
                                this.getUsers();
                                this.form.id = this.form.username = this.form.email = this.form.password = null;
                            }
                        });
                }else{
                    this.$http.post('{{ route('user-edit')  }}',this.form)
                        .then(response => {
                            this.getUsers();
                            this.form.id = this.form.username = this.form.email = this.form.password = null;
                        });
                }
            },
            editUser: function(userId){
                this.$http.get('{{ route('user-edit')  }}',{
                    params:{
                        id: userId
                    }
                }).then(response => {
                    this.form.id = response.data.user.id;
                    this.form.username = response.data.user.username;
                    this.form.email = response.data.user.email;
                    this.form.password = response.data.user.password;
                })
            },
            deleteUser: function(userId){
                if(confirm("Do you really want to delete?")) {
                    this.$http.delete('{{ route('user-delete')  }}',{
                        params:{
                            id : userId
                        }
                    }).then(response => {
                        if (response.data.status) {
                            alert('Record Delete Successfully !');
                            this.getUsers();
                        }
                    });
                }
            }
        },
        mounted: function () {
            this.getUsers();
        }
    });
</script>
</body>
</html>