<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('users','UserController@index')->name('get-users');
Route::post('create','UserController@create')->name('user-create');
Route::get('update','UserController@update')->name('user-edit');
Route::post('update','UserController@update')->name('user-update');
Route::delete('delete','UserController@delete')->name('user-delete');
