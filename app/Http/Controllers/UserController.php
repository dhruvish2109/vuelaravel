<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {

         $users = User::orderBy('id','asc')->get();

        return response()->json(['status'=> 'success', 'users' => $users]);
    }

    public function create(Request $request){

        if($request->isMethod('post')){

            $this->validate($request,[
                'username' => ['required'],
                'email' => ['required'],
                'password' => ['required'],
            ]);

            User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => \Hash::make($request->password)
            ]);

            return response()->json(['status'=>true,'message' => 'User Added Successful !']);
        }
    }

    public function update(Request $request){

        $user = User::whereId($request->id)->first();

        if($request->isMethod('post')) {

            $this->validate($request,[
                'username' => ['required'],
                'email' => ['required'],
                'password' => ['required'],
            ]);

            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = \Hash::make($request->password);
            $user->save();
        }

        return response()->json(['status'=>true,'message' => 'User Update Successful !','user' => $user]);
    }

    public function delete(Request $request){

        $user = User::whereId($request->id)->delete();

        if($user){
            return response()->json(['status'=>true,'message' => 'User Delete Successful !']);
        }
    }
}
